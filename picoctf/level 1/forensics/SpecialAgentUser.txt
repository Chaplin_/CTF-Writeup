Task:
We can get into the Administrator's computer with a browser exploit. But first, we need to figure out what browser they're using. Perhaps this information is located in a network packet capture we took: data.pcap. Enter the browser and version as "BrowserName BrowserVersion".

Solution:

- Download the file and open it with Wireshark
- We need to search for the used Browser and Version
- Enter the filter http.user_agent
- Done
