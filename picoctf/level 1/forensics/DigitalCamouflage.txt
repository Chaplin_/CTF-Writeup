Task: 
We need to gain access to some routers. Let's try and see if we can find the password in the captured network data: data.pcap.

Solution:

- Download "data.pcap"
- Open File with Wireshark
- Enter Filter "http.request.method = POST
- Only 1 paket shows up
- It has the Password, but encoded with Base64
- Decode it (e.g online or with any language)
