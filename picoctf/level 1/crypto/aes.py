"""
ciphertext = t1h0qbcOhRQF5E46bsNLimfbcI6egrKP4LHtKR3lT4UdWjhssM8RQSBT7S/8rcRy
key = T5uVzYtuBNv6vwjohslV4w==
"""

from base64 import b64decode
from Crypto.Cipher import AES

def main():
    key = b64decode("T5uVzYtuBNv6vwjohslV4w==")
    ciphertext = b64decode("t1h0qbcOhRQF5E46bsNLimfbcI6egrKP4LHtKR3lT4UdWjhssM8RQSBT7S/8rcRy")

    deciper = AES.new(key, AES.MODE_ECB)
    plaintext = deciper.decrypt(ciphertext)
    print(plaintext)

if __name__=="__main__":
    main()